# Sensors BME280

Capteur de température, humidité, pression pour la plateforme Mysensors avec un capteur BME280

Matériel :

* Arduino avec Atemega328p
* NRF24l01
* Capteur BME280

## Organisation

Chaque branches que vous pouvez visualiser à travers le menu déroulant au niveau de `master` vous permettra de revenir à un état du projet et continuer votre propre developpement.

|Branches| Etat du projet |
|--|--|
| Base | Projet initial |
| Consommation | Projet optimisé pour consommer moins |
